angular.module('starter.services', [])


//factory for authentication and login signup 
.factory('HomeOwners', function($http, $q, ApiEndpoint) {
    console.log(ApiEndpoint);
    var baseurl = ApiEndpoint.url;
    return {

  /*....................  start Create auction service.................................... */
        Create: function(item) {
            //alert(item);
            var Url = baseurl + 'homeowners/auctions';

            var defer = $q.defer();
            console.log(item);
            $http.post(Url, item).
            success(function(data, status, headers, config) {
                console.log(data);
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },
        
 /*....................  End Create auction service.................................... */
 
 /*....................  start Card submit service.................................... */
        
        NewCard: function(data) {


            var Url = baseurl + 'card/create';

            var defer = $q.defer();

            $http.post(Url, data).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },
 /*....................  End Card submit service.................................... */
 
 reply_dispute: function(item,id) {


            var Url = baseurl + 'disputes/'+id+'/resolve?api_token='+item.api_token;

            var defer = $q.defer();

            $http.post(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },
 /*....................  Start Current Bid service.................................... */
 
        Bid: function(item) {

            var Url = baseurl + 'homeowner/my_bids?api_token=' + item.api_token;

            var defer = $q.defer();

            $http.get(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },
 /*....................  End Current Bid service.................................... */
 
 /*....................  start review api to getting the comapny name service.................................... */
 review_company_name:function(id,item) {

            var Url = baseurl + 'appointments/'+id+'/review_and_pay?api_token=' + item.api_token;

            var defer = $q.defer();

            $http.get(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },
 
 /*.................... end review api to getting the comapny name service.................................... */
 
 
 
 
 
 /*....................  Start Date service.................................... */
 
        Schedule_date: function(item,tokens) {
 
            var Url = baseurl + 'appointments?api_token='+tokens;
 
            var defer = $q.defer();
 
            $http.post(Url,item).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });
 
            return defer.promise;
        },
 /*....................  End Date service.................................... */

 /*....................  Start Bid Info service.................................... */
        BidInfo: function(item) {

            var Url = baseurl + 'homeowner/bidder_info/' + item.bid_id;

            var defer = $q.defer();

            $http.get(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },

 /*....................  End Bid Info service.................................... */


        newlyCreated: function(item) {

            var Url = baseurl + 'homeowner/bidder_info/' + item.bid_id;

            var defer = $q.defer();

            $http.get(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },


 /*....................  Start Bid Accept service.................................... */

        BidAccept: function(item) {

            var Url = baseurl + 'bid/' + item.bid_id + '/accept?api_token='+item.api_token;

            var defer = $q.defer();

            $http.post(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },
 /*....................  End Bid Accept service.................................... */
 
 /*....................  Start credit card info service.................................... */ 
        credit: function(item) {

            var Url = baseurl + 'card/new?api_token=' + item.api_token;

            var defer = $q.defer();

            $http.get(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },

 /*....................  End credit card info service.................................... */ 
 
 /*....................  Start Appointment service.................................... */ 
        Appointment: function(item) {

            var Url = baseurl + 'appointments/all?api_token=' + item.api_token;

            var defer = $q.defer();

            $http.get(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },
 /*....................  End Appointment service.................................... */ 
        checkauctions: function() {
            //alert("!");
            var Url = baseurl + 'homeowner/auctions';

            var defer = $q.defer();

            $http.jsonp(Url).
            success(function(data, status, headers, config) {

                defer.resolve(data);

            }).
            error(function(data, status, headers, config) {
                defer.reject();

            });

            return defer.promise;
        },

 /*....................  Start LoginDetail service.................................... */ 
        logindetail: function(data) {
            var Url = baseurl +'user/session';

            var defer = $q.defer();


            $http.post(Url, data).
            success(function(data, status, headers, config) {

                defer.resolve(data);

            }).
            error(function(data, status, headers, config) {
                console.log(data);
                defer.reject();

            });

            return defer.promise;
        },
 /*....................  End LoginDetail service.................................... */
 
  /*....................  Start Review service.................................... */ 
 
        reviewer: function(data, tokens, id) {

            var Url = baseurl + 'appointments/' + id + '/submit_review_and_payment';

            var defer = $q.defer();


            $http.post(Url, data).
            success(function(data, status, headers, config) {

                defer.resolve(data);

            }).
            error(function(data, status, headers, config) {
                console.log(data);
                defer.reject();

            });

            return defer.promise;
        },
/*....................  End Review service.................................... */ 


/*....................  Start Submit dispute service.................................... */ 
        submit_dispute: function(data, tokens, id) {

            var Url = baseurl + 'appointments/' + id + '/submit_dispute?api_token=' + tokens;

            var defer = $q.defer();


            $http.post(Url, data).
            success(function(data, status, headers, config) {

                defer.resolve(data);

            }).
            error(function(data, status, headers, config) {
                console.log(data);
                defer.reject();

            });

            return defer.promise;
        },
/*....................  End Submit dispute service.................................... */ 

/*....................  Start HomePage service.................................... */ 
        Homepage: function(item) {

            var Url = baseurl + 'homeowner/index?api_token=' + item;

            var defer = $q.defer();

            $http.get(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },

/*....................  End HomePage service.................................... */ 



/*....................  Start mygreenpal service.................................... */ 
        mygreenpal: function(item) {

            var Url = baseurl + '/homeowner/my_greenpal?api_token=' + item;

            var defer = $q.defer();

            $http.get(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },

/*....................  End mygreenpal service.................................... */ 


/*....................  Start schedulemore service.................................... */ 
        scheduleemore: function(item) {

            var Url = baseurl + 'appointment/schedule_more?api_token=' + item;

            var defer = $q.defer();

            $http.get(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;
        },

/*....................  End HomePage service.................................... */ 


/*....................  Start Logout service.................................... */ 
        logoutuser: function(item) {
            //storageService.remove('api_token');
            //return 'nishant';


            var Url = baseurl + 'user/session/logout?api_token=' + item.api_token;

            var defer = $q.defer();

            $http.post(Url).
            success(function(data, status, headers, config) {
                defer.resolve(data);
            }).
            error(function(data, status, headers, config) {
                defer.reject();
            });

            return defer.promise;




        },




    }
/*....................  End Logout service.................................... */ 
})

//localstorage factory
.factory('storageService', function($rootScope) {

    return {

        get: function(key) {
            return localStorage.getItem(key);
        },

        save: function(key, data) {
            localStorage.setItem(key, JSON.stringify(data));
        },

        remove: function(key) {
            localStorage.removeItem(key);
        },

        clearAll: function() {
            localStorage.clear();
        }
    };
})
