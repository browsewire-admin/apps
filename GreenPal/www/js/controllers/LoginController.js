
angular.module('starter.controller', [])

/*.................... start login Controller for vendor and homeowner part.................................... */


.controller('powerCtrl', function($scope, $state, $http, HomeOwners,storageService,$ionicPopover,$rootScope, $ionicPlatform, $cordovaLocalNotification) {

	 $scope.userId={};
	 $scope.useremail={};
	 $scope.userId=storageService.get("userId");
	 console.log( $scope.userId);
	 $scope.useremail=storageService.get("useremail");

	 
	 $ionicPopover.fromTemplateUrl('templates/popover.html', {
		scope: $scope,
	}).then(function(popover) {
 
    $scope.popover = popover;
	
  });

 
			
	 $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);
			 $scope.popover.hide();
            $state.go("login");
        });

    }
			
		

})



.controller('LoginCtrl', function($scope, $state, HomeOwners, storageService, $ionicPopup, $ionicLoading, $cordovaToast,$ionicHistory) {
$ionicHistory.clearCache();



    //initialize user
    $scope.user = {};
    $scope.orginal = {};
	
    ////console.log(test);

	$scope.image_remove=function()
	{
	
	document.getElementById("rmve").style.display="none";
	}
   
   /*...................................... Start Login functionality  .....................................*/
    $scope.login = function() {


        var data = "user[email]=" + encodeURIComponent($scope.user.email) + "&user[password]=" + $scope.user.password;

        $scope.loadingIndicator = $ionicLoading.show({
            content: 'Loading Data',
            animation: 'fade-in',
            showBackdrop: false,
            maxWidth: 200,
            showDelay: 500

        });
	$ionicHistory.clearCache();
        HomeOwners.logindetail(data).then(function(response) {
            var login_api_token = response.api_token;
            //console.log(response);
            storageService.save("api_token", login_api_token);
            var company_id = response.company_id;
			
			
            if (response.api_token != '' && company_id == null) {

                var Api_token = response.api_token;
				
			
				
				console.log(Api_token);
				$ionicHistory.clearCache();
                HomeOwners.Homepage(Api_token).then(function(response) {
                  
					$ionicHistory.clearCache();
                    if (typeof response.open_auction != "undefined") {


                        var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

						$ionicHistory.clearCache();
                        HomeOwners.Bid(data).then(function(response) {
							console.log("test1");
                            if (response.bids != "") {
								console.log("test2");
							$ionicHistory.clearCache();
                                storageService.save("currentBid", response);
								console.log("test3");
                             console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }


                            if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })



                        //$state.go('my_bids');
                    } else if (typeof response.upcoming_appointments != "undefined") {
                        var upcoming_appointments = JSON.parse(response.upcoming_appointments);
                        ////console.log(upcoming_appointments);
						console.log(response);
                        storageService.save("upcoming_appointment", upcoming_appointments);
                        $ionicLoading.hide();
                        $state.go('appointment');
                 
                        
                    } else if (typeof response.completed_appointment != "undefined") {
                        $ionicLoading.hide();

                        var completed_obj = response;
                        storageService.save("completed", completed_obj);
						$state.go('lawn_cut');
                      //$state.go('appointment');
                    } else if (typeof response.dispute != "undefined") {

                        $ionicLoading.hide();
                        var disputed_obj = response;
						
                        storageService.save("disputed", disputed_obj);
						if(response.dispute_resolution_photo==null)
						{
                        $state.go('cut_in_dispute');
						}
						else
						{
						console.log(response);
							 $state.go('dispute_reply');
						}
                    } else {
                        $ionicLoading.hide();
						console.log(response);
						$scope.new_response=response.message;
						storageService.save("dashctrlresponse", $scope.new_response);
						if(response.errors=="No Appointments or Auctions found!")
						{
						$state.go('no_appointment');
						}
						
						if(response.message=="You Do Not Have an Auction or Any Appointments")
						{
						var api_token = storageService.get("api_token");
						$scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

						HomeOwners.Bid(data).then(function(response) {
							console.log("test1");
                            if (response.bids != "") {
								console.log("test2");
							$ionicHistory.clearCache();
                                storageService.save("currentBid", response);
								console.log("test3");
                             console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
							
                                $ionicLoading.hide();
                                $state.go('dash');
                            }
							
							 if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })

						}
						else
						{
						
						$state.go('no_appointment');
						}
                        
                    }
                })
            }



            if (response.api_token != '' && company_id != null) {
                $ionicLoading.hide();
              $state.go('overdue');
               
            }

        }, function(error) {

            $cordovaToast.show('Wrong Credentials', 'long', 'center')
                .then(function(success) {

            }, function(error) {

            });
            $ionicLoading.hide();

        })



    }
       /*...................................... End Login functionality  .....................................*/
    //reset password
    $scope.resetPassword = function() {
        //$ionicLoading.hide();
        //$state.go('reset_password');
		var item="https://www.yourgreenpal.com/new_homeowner_signup";
		window.open(item, "_blank", "location=yes");

    }

})

/*.................... End login Controller for vendor and homeowner part.................................... */


/*.................... start Display Current Bids for  homeowner part.................................... */

.controller('BidsCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService,$ionicHistory) {

$ionicHistory.nextViewOptions({disableBack: true});

    //$scope.orginal  = {};
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    //$scope.rate = 3;
    var datasets = [];
    $scope.ratings = {};
    $scope.max = 5;
	$scope.message = {};
    $scope.full_response={};
	$scope.greenpal={};
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];


	
    var current_Bid = JSON.parse(storageService.get("currentBid"));
	console.log(current_Bid);
    var bids = current_Bid.bids;
    
    if(bids==null)
    {  
		
		$state.go("dash");
	}
	else
	{
	$scope.allBids = bids;	
    $scope.ratings = $scope.allBids.company_rating;
	}
    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);
            $state.go("login");
        });

    }
	$scope.changetab = function(item) {
        $scope.tab = item;

    }
		var api_token = storageService.get("api_token");
        var Api_token = JSON.parse(api_token);
		HomeOwners.mygreenpal(Api_token).then(function(response) {
                  
				  
				  console.log(response);
				  if(response.message=="This is your current GreenPal")
				  {
				  $scope.message=response.message;
				  $scope.greenpal=JSON.parse(response.greenpal);
				  $scope.full_response=response;
				  }
				  if(response.message=="You Do Not Have a Current GreenPal")
				  {
				  $scope.message=response.message;
				  }
				  if(response.message=="You Do Not Have an Auction or Any Appointments")
				  {
				  $scope.message='';
				  }
				  
				  })
	
	
})


/*.................... End Display Current Bids for  homeowner part.................................... */



/*.................... start Display Selected Bid With Info for  homeowner part.................................... */

.controller('BidInfoCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams,$ionicLoading,$ionicHistory) {
    //alert("11111");
	$ionicHistory.nextViewOptions({disableBack: true});
    $scope.orginal = {};
    $scope.ratings = {};
    $scope.reviews_count = {};
    //var photos  =  []; 
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    //$scope.rate = 3;
    $scope.max = 5;
    var i;
    var answerValue = 0;
    var datasets = [];
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];
	
	 $scope.previousPage = function() {
	  //console.log('here');
	  //$ionicHistory.goBack();
	  $state.go("my_bids");
	}
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token
    };
    var bid_id = $stateParams.BidderId;
    $scope.current_bid = bid_id;
    //alert(bid_id);
    var json = {
        'bid_id': bid_id
    };
    
       /*...................................... Start Specific BidInfo Service  .....................................*/
    HomeOwners.BidInfo(json).then(function(response) {

        $scope.bidInfos = response.company
        console.log(response);

        var bids = response.bid;
        var work_photos = response.work_photos;

        var reviews = response.reviews;
        if (reviews != '') {

            var reviews_count = reviews.length;

            angular.forEach(reviews, function(key, value) {

                var rating = key.rating;
                datasets.push(rating);


            })
            //  //console.log(datasets);

            for (i = 0; i < reviews_count; i++) {
                answerValue += Number(datasets[i]);
            }
            var rate_avg = Math.round(answerValue / reviews_count);

        }
        $scope.ratings = rate_avg;
        $scope.reviews_count = reviews_count;

        $scope.allBidsInfo = {
            'bids': bids,
            'reviews': reviews,
            'reviews_count': reviews_count,
            'rate_avg': $scope.rate,
            'work_photos': work_photos,
            'avatar': response.avatar
        };
       
    })
       /*...................................... End Specific BidInfo Service  .....................................*/
      
       /*...................................... Start Accept Bid Service  .....................................*/       
    $scope.pickMe = function(id) {
	
		
        var api_token = storageService.get("api_token");
        $scope.orginal = JSON.parse(api_token);
        var current_bid = {
            'bid_id': id,
            'api_token': $scope.orginal
        };

       /*...................................... End Accept Bid Service  .....................................*/ 
	   
	   
	   HomeOwners.BidAccept(current_bid).then(function(newresponse) {
	   console.log(newresponse);
	   })

       /*...................................... Start Credit Card Service  .....................................*/     

        HomeOwners.credit(current_bid).then(function(response) {
            console.log(response);
             
            //console.log(response.homeowner.last_4_of_card_on_file);
            $scope.current_bid = bid_id;
            //alert(bid_id);

            storageService.save("bid_id", current_bid);
            //  $state.go('secure_date');
            //$location.path('/flickrs/');

            if (response.homeowner.last_4_of_card_on_file!= null) {
				storageService.save("bid_accpt_id",id);
               // $state.go('main');
			   
			   
			   /* client ask to move here from dasboard */
			       var api_token = storageService.get("api_token");
					$scope.orginal = JSON.parse(api_token);
			var Api_token = $scope.orginal;
			console.log(Api_token);
			console.log("nishant");
			HomeOwners.Homepage(Api_token).then(function(response) {
                  

                    if (typeof response.open_auction != "undefined") {


                        var api_token = storageService.get("api_token");
                        $scope.orginal = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal
                        };

                        HomeOwners.Bid(data).then(function(response) {

                            if (response.bids != "") {
                                storageService.save("currentBid", response);
                                ////console.log(response);
								
                                $ionicLoading.hide();
								
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
								
                                $state.go('dash');
                            }


                            if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })



                        //$state.go('my_bids');
                    } else if (typeof response.upcoming_appointments != "undefined") {
                        var upcoming_appointments = JSON.parse(response.upcoming_appointments);
                        ////console.log(upcoming_appointments);
                        storageService.save("upcoming_appointment", upcoming_appointments);
                        $ionicLoading.hide();
                        $state.go('appointment');
                 
                        
                    } else if (typeof response.completed_appointment != "undefined") {
                        $ionicLoading.hide();

                        var completed_obj = response;
                        storageService.save("completed", completed_obj);
						$state.go('lawn_cut');
                      //$state.go('appointment');
                    } else if (typeof response.dispute != "undefined") {

                        $ionicLoading.hide();
                        var disputed_obj = response;
						
                        storageService.save("disputed", disputed_obj);
						if(response.dispute_resolution_photo==null)
						{
                        $state.go('cut_in_dispute');
						}
						else
						{
						console.log(response);
							 $state.go('dispute_reply');
						}
                    } else {
                        /*$ionicLoading.hide();
						console.log(response);
						
						$state.go('no_appointment');*/
						
						$ionicLoading.hide();
						console.log(response);
						
						if(response.errors=="No Appointments or Auctions found!")
						{
						$state.go('no_appointment');
						}
						
						if(response.message=="You Do Not Have an Auction or Any Appointments")
						{
						 var api_token = storageService.get("api_token");
                        $scope.orginal = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal
                        };

						HomeOwners.Bid(data).then(function(response) {
							console.log("test1");
                            if (response.bids != "") {
								console.log("test2");
							$ionicHistory.clearCache();
                                storageService.save("currentBid", response);
								console.log("test3");
                             console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }
							
							 if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })

						}
						else
						{
						
						$state.go('no_appointment');
						}
						
						
						
                        
                    }
                })
			
			/* end client ask to move here from dasboard */
			
			   
			   
			   
			   
            } else {

                $state.go('secure_date');
            }

        })

       /*...................................... End Credit Card Service  .....................................*/     

    }

    var bid_id = $stateParams.BidderId;

    var current_bid = {
        'bid_id': bid_id
    };
    //$state.go('provider_detail/'+data);

    HomeOwners.BidInfo(current_bid).then(function(response) {
   


    })



    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }


})


/*.................... End Display Selected Bid With Info for  homeowner part.................................... */


/*.................... start credit card submit for  homeowner part.................................... */

.controller('creditCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams,$ionicLoading,$ionicHistory) {
	$ionicHistory.clearCache();
    $scope.orginal = {};
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    $scope.rate = 3;
    $scope.max = 5;
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token
    };

    $scope.changetab = function(item) {
        $scope.tab = item;

    }

    /*=======Stripe Starts Here==========*/
    $scope.stripeToken = {};
    $scope.appointment = {};
    $scope.card = function() {

        var number = document.getElementById('number').value;
		var n =number.length;
		
        var expiry = document.getElementById('exp').value;
		var expy=expiry.length;
		if(expy!=0)
		{
        var res = expiry.split("/");
        var month = res[0];
        var year = res[1];
		}
        var cvv = document.getElementById('cvc').value;
		var cv =cvv.length;
		
		
		
		 $scope.loadingIndicator = $ionicLoading.show({
            content: 'Loading Data',
            animation: 'fade-in',
            showBackdrop: false,
            maxWidth: 200,
            showDelay: 500

			});
			
			
		if(n<13)
{
var alertPopup = $ionicPopup.alert({

                template: 'Invalid Card Number'
               });
             alertPopup.then(function(res) {
                 ////console.log('Thank you for not eating my delicious ice cream cone');
               });
			   $ionicLoading.hide();
}




		else if(cv<3)
{
var alertPopup = $ionicPopup.alert({
                template: 'CVC Number Is Invalid'
               });
             alertPopup.then(function(res) {
                 ////console.log('Thank you for not eating my delicious ice cream cone');
               });
			   $ionicLoading.hide();
}



		else if(expy<4)
{
		var alertPopup = $ionicPopup.alert({
                template: 'Card Expiry Date Is Invalid'
               });
             alertPopup.then(function(res) {
                 ////console.log('Thank you for not eating my delicious ice cream cone');
               });
			   
			   $ionicLoading.hide();
}


	
else
{
//$ionicLoading.hide();

       /*...................................... Start Stripe functionality  .....................................*/     

		  $scope.stripeCallback = function(code, result) {

           console.log(result);
            if (result.error) {
                $scope.stripeError = result.error.message;
            } else {
                $scope.stripeToken = result.id;
            }

            var api_token = storageService.get("api_token");
            $scope.orginal.api_token = JSON.parse(api_token);
           var api_tokens=$scope.orginal.api_token;
            var data = {
                accepting_bid_id: 66
            };
            var stripeToken =$scope.stripeToken;

            //console.log(result.card.exp_month);
	var stripe_data = "number=" + number + "&exp-month=" + result.card.exp_month +  "&exp-year=" + result.card.exp_year +  "&cvc=" + cvv + "&stripeToken=" + stripeToken + "&api_token=" + $scope.orginal.api_token;
  
            HomeOwners.NewCard(stripe_data).then(function(response) {
              console.log(response);
                
			//	storageService.save("bid_accpt_id",response.bid.id);
				
				
				var Api_token = $scope.orginal.api_token;
			HomeOwners.Homepage(Api_token).then(function(response) {
                  

                    if (typeof response.open_auction != "undefined") {


                        var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

                        HomeOwners.Bid(data).then(function(response) {

                            if (response.bids != "") {
                                storageService.save("currentBid", response);
                                ////console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }


                            if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })



                        //$state.go('my_bids');
                    } else if (typeof response.upcoming_appointments != "undefined") {
                        var upcoming_appointments = JSON.parse(response.upcoming_appointments);
                        ////console.log(upcoming_appointments);
                        storageService.save("upcoming_appointment", upcoming_appointments);
                        $ionicLoading.hide();
                        $state.go('appointment');
                 
                        
                    } else if (typeof response.completed_appointment != "undefined") {
                        $ionicLoading.hide();

                        var completed_obj = response;
                        storageService.save("completed", completed_obj);
						$state.go('lawn_cut');
                      //$state.go('appointment');
                    } else if (typeof response.dispute != "undefined") {

                        $ionicLoading.hide();
                        var disputed_obj = response;
						
                        storageService.save("disputed", disputed_obj);
						if(response.dispute_resolution_photo==null)
						{
                        $state.go('cut_in_dispute');
						}
						else
						{
						console.log(response);
							 $state.go('dispute_reply');
						}
                    } else {
                       /* $ionicLoading.hide();
						console.log(response);
						
						$state.go('no_appointment');*/
						
						
						$ionicLoading.hide();
						console.log(response);
						
						if(response.errors=="No Appointments or Auctions found!")
						{
						$state.go('no_appointment');
						}
						
						if(response.message=="You Do Not Have an Auction or Any Appointments")
						{
						 var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

						HomeOwners.Bid(data).then(function(response) {
							console.log("test1");
                            if (response.bids != "") {
								console.log("test2");
							$ionicHistory.clearCache();
                                storageService.save("currentBid", response);
								console.log("test3");
                             console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }
							
							 if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })

						}
						else
						{
						
						$state.go('no_appointment');
						}
						
						
                        
                    }
                })
			
			/* end client ask to move here from dasboard */
			
				
                
               // $state.go('main');
            })
        };

}

}
    /*=========Stripe Ends Here===========*/



    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }



})


/*.................... End credit card submit for  homeowner part.................................... */



/*.................... start  No Appointment page for  homeowner part.................................... */

.controller('NoAppointmentCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams,$ionicHistory,$cordovaEmailComposer,$cordovaSms) {
$ionicHistory.clearCache();
    $scope.orginal = {};
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    $scope.rate = 3;
    $scope.max = 5;
	$scope.message = {};
    $scope.full_response={};
	$scope.greenpal={};
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token
    };

       /*...................................... Start changetab functionality  .....................................*/     
    $scope.changetab = function(item) {
        $scope.tab = item;

    }

	
	/*...................................... Start call function plugin  .....................................*/
    $scope.call = function(phonenumber) {

	var telphone='tel:';
	var telephone_number=telphone+phonenumber;

    navigator.app.loadUrl(telephone_number, {
            openExternal: true
        });

    }

   /*...................................... End call function plugin  .....................................*/
   
   /*...................................... Start email function plugin  .....................................*/
    $scope.email = function(email_id) {
		
        $cordovaEmailComposer.isAvailable().then(function() {
            // is available
        }, function() {
            // not available
        });

        var email = {
            to: email_id,
            subject: 'Greenpal',
            body: 'How are you? Nice greetings from Greenpal',
            isHtml: true
        };

        $cordovaEmailComposer.open(email).then(null, function() {
            // user cancelled email
        });


    }

   /*...................................... End email function plugin  .....................................*/
   
   /*...................................... Start SMS function plugin  .....................................*/   
    $scope.sms = function(phonenumber) {

        document.addEventListener("deviceready", function() {

            var options = {
                replaceLineBreaks: false, // true to replace \n by a new line, false by default
                android: {
                    intent: 'INTENT' // send SMS with the native android SMS messaging
                    //intent: '' // send SMS without open any other app
                }
            };

            $cordovaSms.send(phonenumber, 'SMS content', options)
                .then(function() {
                // Success! SMS was sent
            }, function(error) {
                // An error occurred
            });

        });
    }
	
       /*...................................... End changetab functionality  .....................................*/    
    $scope.gotourl = function(url) {
        // alert(url);
        $state.go(url);

    }
	
						var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);
						console.log($scope.orginal.api_token);
						var api_tokens=$scope.orginal.api_token;
						HomeOwners.scheduleemore(api_tokens).then(function(response) {
						console.log(response);
						$scope.company_detail=response.company;

						//$state.go("login");
						
						});
    
    
        $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }
    
	var api_token = storageService.get("api_token");
        var Api_token = JSON.parse(api_token);
		HomeOwners.mygreenpal(Api_token).then(function(response) {
                  
				  
				  console.log(response);
				  $scope.responses=response;
				  $ionicHistory.clearCache();
				  if(response.message=="This is your current GreenPal")
				  {
				  $ionicHistory.clearCache();
				  $scope.message=response.message;
				  $scope.greenpal=JSON.parse(response.greenpal);
				  $scope.full_response=response;
				 
				  console.log($scope.full_response);
				  
				  }
				  if(response.message=="You Do Not Have a Current GreenPal")
				  {
				  $ionicHistory.clearCache();
				  $scope.message=response.message;
				  }
				  if(response.message=="You Do Not Have an Auction or Any Appointments")
				  {
				  $ionicHistory.clearCache();
				  $scope.message='';
				  }
				  
				  })
	
	
	
})


/*.................... End  No Appointment page for  homeowner part.................................... */


/*.................... start  Main page shown controller for  homeowner part.................................... */

.controller('MainCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams,$ionicHistory) {

    $scope.orginal = {};
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    $scope.rate = 3;
    $scope.max = 5;
    $ionicHistory.clearCache();
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];
    var api_token = storageService.get("api_token");
    var bid_id=JSON.parse(storageService.get("bid_accpt_id"));
    
    
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token,
        'bid_id'	:bid_id
    };
    
     /*...................................... Start BidAccept Service  .....................................*/    
	 HomeOwners.BidAccept(data).then(function(response) {

				$scope.new_appointment=JSON.parse(response.new_appointment);
                //console.log(response);
                
             //   $state.go('main');


            })
    /*...................................... End  BidAccept Service  .....................................*/
  
    $scope.gotourl = function(url) {
        // alert(url);
        $state.go(url);

    }


    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }



})


/*....................  End  Main page shown controller for  homeowner part.................................... */
















/*....................  Start Lawn Cut controller for  homeowner part.................................... */

.controller('LawnCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams) {

    $scope.orginal = {};
    $scope.completed = {};
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    $scope.rate = 3;
    $scope.max = 5;
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token
    };

    
    $scope.gotourl = function(url) {

        $state.go(url);
    }

    $scope.completed = JSON.parse(storageService.get("completed"));
  console.log($scope.completed);
    $scope.completed_appointment = JSON.parse($scope.completed.completed_appointment);


    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }

})
/*....................  End Lawn Cut controller for  homeowner part.................................... */


/*....................  Start Specific Dates controller for  homeowner part.................................... */
.controller('SpecificCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams,$filter,$ionicHistory,$ionicLoading,$window,$location) {
 
    $scope.full_month = {};
    $scope.schedule={};
    $scope.full_app = {};
    $scope.full_app_new_date = {};
    $scope.orginal={};
    $scope.allAppointments_specific_dates = {};
    var new_date = [];
    var daysets  = [];
    var Specific_new_date = [];
    var app_dates;
    var new_dates = [];
    var next_dates;
    var appointment_date = [];
    var new_daysets = [];
    var all_dates = [];
    $scope.myNumber = 10;
	$ionicHistory.clearCache();
	
	var currentDate2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
var day2 = currentDate2.getDate();
var month2 = currentDate2.getMonth() + 1;
var year2 = currentDate2.getFullYear();
if(day2<10){
        day2='0'+day2;
    } 
    if(month2<10){
        month2='0'+month2;
    } 
$scope.formated_date=year2 + "-" + month2 + "-" + day2;
console.log($scope.formated_date);

	
    
    $scope.gotourl = function(url) {
        // alert(url);
        $state.go(url);
 
    }
 
    var upcoming_appointment = storageService.get("upcoming_appointment");
    $scope.allAppointments_specific_dates = JSON.parse(upcoming_appointment);
 console.log($scope.allAppointments_specific_dates);
 
    angular.forEach($scope.allAppointments_specific_dates, function(key, value) {
 
        var app_dates = new Date(key.service_date);
        //  alert(id);
        appointment_date.push(app_dates);
 
 
    })
 
 
    $scope.full_app = new_date;
    //$scope.full_app_new_date=new_dates;
    $scope.full_month = daysets;
    //console.log($scope.full_app);
 
 
$scope.getNumber = function(num) {
     
       return new Array(num);   
       
 
    }
 
$scope.check_dates=function(index)
{
    ////console.log(dates);
     var first_dates = [];
     var full_Upcoming_wk = [];
     all_dates = [];
     Specific_new_date = [];
     //console.log($scope.schedule);
    
     angular.forEach($scope.schedule, function(key, value) {
        //console.log(key);
        if(value==='dates_'+index)
          {   
             first_dates.push($filter('date')(key, "yyyy-MM-dd"));
          }         
          else
          {
               ////console.log(dates); 
               all_dates.push($filter('date')(key, "yyyy-MM-dd"));
              
                var current_date=$filter('date')(key, "yyyy-MM-dd");
                
                var current_date1 = new Date(current_date);
                
                var current_date_year = current_date1.getFullYear();
                var current_date_month = current_date1.getMonth();
                var current_date_date = current_date1.getDate();
              
               for (var i = 0; i < 6; i++) {
                var wk_first_date = new Date(current_date_year, current_date_month, current_date_date + i);
                var wk_first_dates = new Date(current_date_year, current_date_month, current_date_date - i);
                // //console.log(day);
               var new_current_date=$filter('date')(wk_first_date, "yyyy-MM-dd");
                var new_current_dates=$filter('date')(wk_first_dates, "yyyy-MM-dd");
             
                Specific_new_date.push(new_current_date);
                Specific_new_date.push(new_current_dates);
            
          }
       }  
      });
      
      console.log(Specific_new_date);
      
      
      var todays_dates = new Date();
      var new_todays_dates=$filter('date')(todays_dates, "yyyy-MM-dd");

      //check if selected date match to today' date
      if(first_dates.indexOf(new_todays_dates) !== -1)
      {
          $scope.schedule['dates_'+index]='';
          var alertPopup = $ionicPopup.alert({     
               template: 'you can\'t  select today\'s date'
          });
           alertPopup.then(function(res) {
             ////console.log('Thank you for not eating my delicious ice cream cone');
           });
     }
     else
     {
         //if date does not match
         //console.log(all_dates);
         var new_dates = $filter('date')($scope.schedule['dates_'+index], "yyyy-MM-dd");
         
         if(all_dates.length > 1)
         {
            //if date matched from previously added dates 
           if(all_dates.indexOf(new_dates)!==-1)
                 {
                      //console.log("yes");
                   $scope.schedule['dates_'+index]='';
                   var alertPopup = $ionicPopup.alert({
                             template: 'Date already selected'
                           });
                   alertPopup.then(function(res) {
                    
                   });
 
     }
     else
     {

         var new_coming_dates=$filter('date')($scope.schedule['dates_'+index], "yyyy-MM-dd");
         // check if date comes in b/w 6 days
         if(Specific_new_date.indexOf(new_coming_dates)!==-1)
         {
              //console.log("yes");
             $scope.schedule['dates_'+index]='';
                var alertPopup = $ionicPopup.alert({
                template: 'Can\'t select two dates in a week'
               });
             alertPopup.then(function(res) {
                 ////console.log('Thank you for not eating my delicious ice cream cone');
               });
     
        }
     
      }
     
     }
     
  }
     
}    
    
$scope.schedule_data=function()
{
      var api_token = storageService.get("api_token");
        $scope.orginal.api_token = JSON.parse(api_token);
        var api_tokens = $scope.orginal.api_token;
    //console.log($scope.schedule);
    //console.log(api_tokens);
      var dates = [];
     
     angular.forEach($scope.schedule, function(key, value) {
        //console.log(key); 
        dates.push($filter('date')(key, "yyyy-MM-dd"));
      });
      var new_dates_array=dates.toString();
	 // console.log(new_dates_array);
	  //console.log(dates);
	  //console.log(JSON.stringify(dates));
	  //var date="2016-01-12,2015-01-10";
      var data="new_dates=" +  new_dates_array;
      
      //console.log(data); 
     
      HomeOwners.Schedule_date(data,api_tokens).then(function(responses) {
 
                 
               console.log(responses);
               //$ionicHistory.goBack();
             //   $state.go('main');
			 
			 
			 /* client ask to move here from dasboard */
			var Api_token = $scope.orginal.api_token;
			HomeOwners.Homepage(Api_token).then(function(response) {
                  

                    if (typeof response.open_auction != "undefined") {


                        var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

                        HomeOwners.Bid(data).then(function(response) {

                            if (response.bids != "") {
                                storageService.save("currentBid", response);
                                ////console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }


                            if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })



                        //$state.go('my_bids');
                    } else if (typeof response.upcoming_appointments != "undefined") {
					console.log(response.upcoming_appointments);
                        var upcoming_appointments = JSON.parse(response.upcoming_appointments);
                        console.log(upcoming_appointments);
					
						storageService.remove("upcoming_appointment");
                        storageService.save("upcoming_appointment", upcoming_appointments);
                      //  $ionicLoading.hide();
						
						$ionicHistory.clearCache();
                        $state.go('appointment');
                 
                        
                    } else if (typeof response.completed_appointment != "undefined") {
                        $ionicLoading.hide();

                        var completed_obj = response;
                        storageService.save("completed", completed_obj);
						$state.go('lawn_cut');
                      //$state.go('appointment');
                    } else if (typeof response.dispute != "undefined") {

                        $ionicLoading.hide();
                        var disputed_obj = response;
						
                        storageService.save("disputed", disputed_obj);
						if(response.dispute_resolution_photo==null)
						{
                        $state.go('cut_in_dispute');
						}
						else
						{
						console.log(response);
							 $state.go('dispute_reply');
						}
                    } else {
                       /* $ionicLoading.hide();
						console.log(response);
						
						$state.go('no_appointment');*/
						
						$ionicLoading.hide();
						console.log(response);
						
						if(response.errors=="No Appointments or Auctions found!")
						{
						$state.go('no_appointment');
						}
						
						if(response.message=="You Do Not Have an Auction or Any Appointments")
						{
						 var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

						HomeOwners.Bid(data).then(function(response) {
							console.log("test1");
                            if (response.bids != "") {
								console.log("test2");
							$ionicHistory.clearCache();
                                storageService.save("currentBid", response);
								console.log("test3");
                             console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }
							
							 if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })

						}
						else
						{
						
						$state.go('no_appointment');
						}
						
						
                        
                    }
                })
			
			/* end client ask to move here from dasboard */
			
 
 
            })
     
}
 
 
 
     $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }
 
 
}) 


/*....................  End Specific Dates controller for  homeowner part.................................... */


/*....................  Start Dispute controller for  homeowner part.................................... */

.controller('DisputeCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams) {
    $scope.orginal = {};
    $scope.disputed = {};
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    $scope.rate = 3;
    $scope.max = 5;
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token
    };


    $scope.gotourl = function(url) {
        // alert(url);
        $state.go(url);
    }


    $scope.disputed = storageService.get("disputed");

    $scope.disputed_data = JSON.parse($scope.disputed);
    $scope.disputed_data_disputed_appointment = JSON.parse($scope.disputed_data.disputed_appointment);
    //console.log($scope.disputed_data);



    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }

})


/*....................  End Dispute  controller for  homeowner part.................................... */


/*....................  Start DisputeImage controller for  homeowner part when open the completed appointment after login.................................... */
.controller('DisputeImageCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams, $cordovaCamera, $cordovaActionSheet,$ionicLoading) {

    $scope.orginal = {};
    $scope.disputed = {};
    $scope.tab = 'first';
    $scope.disputeId = {};
	//$scope.imgURI={};
	$scope.imgURl={};

    $scope.disputeId = $stateParams.disputeId;

 /*...................................... Start Camera Plugin  .....................................*/
    $scope.Picture = function() {

        var options = {
            title: 'Choose Option',
            buttonLabels: ['Take Picture', 'Upload From Gallery'],
            addCancelButtonWithLabel: 'Cancel',
            androidEnableCancelButton: true,
            winphoneEnableCancelButton: true

        };

        document.addEventListener("deviceready", function() {

            $cordovaActionSheet.show(options)
                .then(function(btnIndex) {
                var index = btnIndex;
                //console.log(index);
                if (index === 1) {
                    var options = {
                        quality: 75,
                        destinationType: Camera.DestinationType.DATA_URL,
                        sourceType: Camera.PictureSourceType.CAMERA,
                        allowEdit: true,
                        encodingType: Camera.EncodingType.JPEG,
                        targetWidth: 300,
                        targetHeight: 300,
                        popoverOptions: CameraPopoverOptions,
                        saveToPhotoAlbum: false
                    };

                    $cordovaCamera.getPicture(options).then(function(imageData) {
                        //console.log(imageData);
                        $scope.imgURI = "data:image/jpeg;base64," + imageData;
						$scope.imgURL = "data:image/jpeg;base64," + imageData;
                    }, function(err) {

                    });

                } else if (index === 2) {
                    var options = {
                       quality: 20,
						targetWidth: 200,
                        targetHeight: 200,
                        destinationType: Camera.DestinationType.FILE_URI,
                        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
                    };
                    $cordovaCamera.getPicture(options).then(function(imageData) {
                        //console.log(imageData);
						
						
						function convertImgToBase64URL(url, callback, outputFormat){
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function(){
        var canvas = document.createElement('CANVAS'),
        ctx = canvas.getContext('2d'), dataURL;
        canvas.height = this.height;
        canvas.width = this.width;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        callback(dataURL);
        canvas = null; 
    };
    img.src = url;
}

convertImgToBase64URL(imageData, function(base64Img){
console.log(base64Img);

 $scope.imgURL = base64Img;
 $scope.imgURI = imageData;                        
    // Base64DataURL
})					
							
							
							
						
						
						
						
						
						
                       

                    }, function(err) {
                        // An error occured. Show a message to the user
                    });

                }
            });
        }, false);
    }
 /*...................................... End Camera Plugin  .....................................*/
 
  /*...................................... Start Submit Dispute functionality  .....................................*/
    $scope.dispute_submit = function() {

        $scope.disputeId = $stateParams.disputeId;
        var id = $scope.disputeId;
        var api_token = storageService.get("api_token");
        $scope.orginal.api_token = JSON.parse(api_token);
        var api_tokens = $scope.orginal.api_token;
        var image = $scope.imgURL;
        var descrip = document.getElementById("descrip").value;
        var data = "image=" + encodeURIComponent(image) + "&description=" + descrip;

        HomeOwners.submit_dispute(data, api_tokens, id).then(function(response) {
            console.log(response);
           // $state.go("cut_in_dispute");
		   
		   
		   
		   
		   var Api_token = $scope.orginal.api_token;
			HomeOwners.Homepage(Api_token).then(function(response) {
                  

                    if (typeof response.open_auction != "undefined") {


                        var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

                        HomeOwners.Bid(data).then(function(response) {

                            if (response.bids != "") {
                                storageService.save("currentBid", response);
                                ////console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }


                            if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })



                        //$state.go('my_bids');
                    } else if (typeof response.upcoming_appointments != "undefined") {
					console.log(response.upcoming_appointments);
                        var upcoming_appointments = JSON.parse(response.upcoming_appointments);
                        console.log(upcoming_appointments);
					
						storageService.remove("upcoming_appointment");
                        storageService.save("upcoming_appointment", upcoming_appointments);
                      //  $ionicLoading.hide();
						
						$ionicHistory.clearCache();
                        $state.go('appointment');
                 
                        
                    } else if (typeof response.completed_appointment != "undefined") {
                        $ionicLoading.hide();

                        var completed_obj = response;
                        storageService.save("completed", completed_obj);
						$state.go('lawn_cut');
                      //$state.go('appointment');
                    } else if (typeof response.dispute != "undefined") {

                        $ionicLoading.hide();
                        var disputed_obj = response;
						
                        storageService.save("disputed", disputed_obj);
						if(response.dispute_resolution_photo==null)
						{
                        $state.go('cut_in_dispute');
						}
						else
						{
						console.log(response);
							 $state.go('dispute_reply');
						}
                    } else {
                       /* $ionicLoading.hide();
						console.log(response);
						
						$state.go('no_appointment');*/
						
						
						$ionicLoading.hide();
						console.log(response);
						
						if(response.errors=="No Appointments or Auctions found!")
						{
						$state.go('no_appointment');
						}
						
						if(response.message=="You Do Not Have an Auction or Any Appointments")
						{
						 var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

						HomeOwners.Bid(data).then(function(response) {
							console.log("test1");
                            if (response.bids != "") {
								console.log("test2");
							$ionicHistory.clearCache();
                                storageService.save("currentBid", response);
								console.log("test3");
                             console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }
							
							 if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })

						}
						else
						{
						
						$state.go('no_appointment');
						}
						
                        
                    }
                })

		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   

        });

    }

 /*...................................... End Submit Dispute functionality  .....................................*/


    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }

})


/*....................  End DisputeImage controller for  homeowner part when open the completed appointment after login.................................... */


/*....................  Start Logout Controller.................................... */


/*....................  End Logout Controller.................................... */

/*....................  start Bc Controller.................................... */

.controller('BcCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams) {

    $scope.orginal = {};
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    $scope.rate = 3;
    $scope.max = 5;
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token
    };
 /*...................................... Start changetab functionality  .....................................*/
    $scope.changetab = function(item) {
        $scope.tab = item;

    }
 /*...................................... End changetab functionality  .....................................*/
    $scope.gotourl = function(url) {
        // alert(url);
        $state.go(url);
    }


    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }


})



/*....................  End Bc Controller.................................... */



/*....................  start Appointment Controller.................................... */
.controller('AppointmentCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams,$ionicModal,$ionicHistory,$cordovaEmailComposer,$cordovaSms) {

    $scope.orginal = {};
    $scope.allAppointments = {};
	$scope.price={};
    var daysets = [];
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    $scope.rate = 3;
    $scope.max = 5;
	$scope.message = {};
    $scope.full_response={};
	$scope.greenpal={};
	$ionicHistory.clearCache();
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token
    };
	
	$ionicHistory.clearCache();
    var upcoming_appointmenting = storageService.get("upcoming_appointment");
    $scope.allAppointments = JSON.parse(upcoming_appointmenting);
		
		

    console.log($scope.allAppointments);
 /*...................................... Start moveslider functionality  .....................................*/
    $scope.moveslider = function(scrollnumber) {
	console.log(scrollnumber);
        var scrollnumber = scrollnumber;
        var scrollnumbers = scrollnumber * 48 - 48;
        var scroll = $scope.scroll_data + 48;
        if (scroll <= scrollnumbers) {
            var position = $ionicScrollDelegate.$getByHandle('small').getScrollPosition();
            console.log(position);

            $ionicScrollDelegate.$getByHandle('small').scrollTo(0, scroll);
            $scope.scroll_data = scroll;
        }
    }
 /*...................................... End moveslider functionality  .....................................*/
    $scope.gotourl = function(url) {
        // alert(url);
        $state.go(url);

    }
    
	
	 angular.forEach($scope.allAppointments, function(key, value) {
	 $scope.price=key.price;
	 })

	 
	  /*...................................... Start call function plugin  .....................................*/
    $scope.call = function(phonenumber) {

	var telphone='tel:';
	var telephone_number=telphone+phonenumber;

    navigator.app.loadUrl(telephone_number, {
            openExternal: true
        });

    }

   /*...................................... End call function plugin  .....................................*/
   
   /*...................................... Start email function plugin  .....................................*/
    $scope.email = function(email_id) {
		
        $cordovaEmailComposer.isAvailable().then(function() {
            // is available
        }, function() {
            // not available
        });

        var email = {
            to: email_id,
            subject: 'Greenpal',
            body: 'How are you? Nice greetings from Greenpal',
            isHtml: true
        };

        $cordovaEmailComposer.open(email).then(null, function() {
            // user cancelled email
        });


    }

   /*...................................... End email function plugin  .....................................*/
   
   /*...................................... Start SMS function plugin  .....................................*/   
    $scope.sms = function(phonenumber) {

        document.addEventListener("deviceready", function() {

            var options = {
                replaceLineBreaks: false, // true to replace \n by a new line, false by default
                android: {
                    intent: 'INTENT' // send SMS with the native android SMS messaging
                    //intent: '' // send SMS without open any other app
                }
            };

            $cordovaSms.send(phonenumber, 'SMS content', options)
                .then(function() {
                // Success! SMS was sent
            }, function(error) {
                // An error occurred
            });

        });
    }

    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }

  $scope.changetab = function(item) {
        $scope.tab = item;

    }
	
	var api_token = storageService.get("api_token");
        var Api_token = JSON.parse(api_token);
		HomeOwners.mygreenpal(Api_token).then(function(response) {
                  
				  
				  console.log(response);
				  if(response.message=="This is your current GreenPal")
				  {
				  $scope.message=response.message;
				  $scope.greenpal=JSON.parse(response.greenpal);
				  $scope.full_response=response;
				  
				  }
				  if(response.message=="You Do Not Have a Current GreenPal")
				  {
				  $scope.message=response.message;
				  }
				  if(response.message=="You Do Not Have an Auction or Any Appointments")
				  {
				  $scope.message='';
				  }
				  
				  })
	
	
	
})

/*....................  End Appointment Controller.................................... */


/*....................  start GreenpalCtrl Controller.................................... */

.controller('GreenpalCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams) {

    $scope.orginal = {};
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    $scope.rate = 3;
    $scope.max = 5;
	  
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token
    };


    $scope.changetab = function(item) {
        $scope.tab = item;

    }

    $scope.gotourl = function(url) {
        // alert(url);
        $state.go(url);
    }


    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }


})

/*....................  End GreenpalCtrl Controller.................................... */


/*....................  start Dashboard Controller.................................... */

.controller('DashCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService) {

	var results=storageService.get("dashctrlresponse");
	$scope.result=results.replace(/\"/g, "")
    $scope.orginal = {};
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    $scope.rate = 3;

    $scope.max = 5;
      var current_Bid = JSON.parse(storageService.get("currentBid"));
	  $scope.result=current_Bid.message;
	  console.log(current_Bid);
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];


  /*  $scope.changetab = function(item) {
        $scope.tab = item;
    }*/
 

    $scope.gotourl = function(url) {
        // alert(url);
        $state.go(url);

    }

    //scroll
 /*...................................... Start moveslider functionality  .....................................*/ 
    $scope.moveslider = function(scrollnumber) {
        var scrollnumber = scrollnumber;
        var scrollnumbers = scrollnumber * 100 - 200;
        var scroll = $scope.scroll_data + 100;
        if (scroll <= scrollnumbers) {
            var position = $ionicScrollDelegate.$getByHandle('small').getScrollPosition();
            //console.log(position);

            $ionicScrollDelegate.$getByHandle('small').scrollTo(0, scroll);
            $scope.scroll_data = scroll;
        }
    }
 /*...................................... End moveslider functionality  .....................................*/ 
 
     $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }
 
 
})

/*....................  End Dashboard Controller.................................... */


/*....................  start New Auction Controller.................................... */

.controller('NewAucCtrl', function($scope, $state, $http, HomeOwners, storageService, $filter,$ionicLoading,$ionicPopup,$ionicHistory) {

    //auction initialize
    $scope.auction = {};
	$scope.orginal={};
    //back to dashboard
    $scope.myGoBack = function() {
        $state.go('dash');
    };

    //set flexibility to 0 default
    $scope.auction.flexibility = 0;

    //toggle flexibility from 0 to 3 
    $scope.changeflex = function() {

        if ($scope.auction.flexibility == 3) {
            $scope.auction.flexibility = -1;
        }
        $scope.auction.flexibility = $scope.auction.flexibility + 1;
    }

    //create auction
    $scope.create_auction = function() {

        var api_token = storageService.get("api_token");
        $scope.auction.api_token = JSON.parse(api_token);
        ////console.log($scope.auction);
         $scope.date_change = $filter('date')($scope.auction.service_date, "yyyy-MM-dd");

        var data = "service_date=" +  $scope.date_change + "&flexibility=" + $scope.auction.flexibility + "&comments=" + $scope.auction.comments + "&api_token=" + $scope.auction.api_token;

        //console.log(data);

        HomeOwners.Create(data).then(function(response) {
            console.log(response);
			 console.log("nishant");
        });

        var api_token = storageService.get("api_token");
        $scope.api_token = JSON.parse(api_token);
        var Ndata = {
            'api_token': $scope.api_token
        };
		var Api_token=$scope.api_token;
		
        /*HomeOwners.Bid(Ndata).then(function(response) {
            //console.log(response);

            if (response.message == "" || response.bids != "") {
                $state.go('my_bids');
            } else {
                $state.go('dash');
            }
        })*/
		/* client ask to move here from dasboard */
$ionicHistory.clearCache();
		HomeOwners.Homepage(Api_token).then(function(response) {
                  

                    if (typeof response.open_auction != "undefined") {

$ionicHistory.clearCache();
                        var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };
$ionicHistory.clearCache();
                        HomeOwners.Bid(data).then(function(response) {
$ionicHistory.clearCache();
                            if (response.bids != "") {
                                storageService.save("currentBid", response);
                                ////console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }


                            if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })



                        //$state.go('my_bids');
                    } else if (typeof response.upcoming_appointments != "undefined") {
                        var upcoming_appointments = JSON.parse(response.upcoming_appointments);
                        ////console.log(upcoming_appointments);
                        storageService.save("upcoming_appointment", upcoming_appointments);
                        $ionicLoading.hide();
                        $state.go('appointment');
                 
                        
                    } else if (typeof response.completed_appointment != "undefined") {
                        $ionicLoading.hide();

                        var completed_obj = response;
                        storageService.save("completed", completed_obj);
						$state.go('lawn_cut');
                      //$state.go('appointment');
                    } else if (typeof response.dispute != "undefined") {

                        $ionicLoading.hide();
                        var disputed_obj = response;
						
                        storageService.save("disputed", disputed_obj);
						if(response.dispute_resolution_photo==null)
						{
                        $state.go('cut_in_dispute');
						}
						else
						{
						console.log(response);
							 $state.go('dispute_reply');
						}
                    } else {
                       /* $ionicLoading.hide();
						console.log(response);
						
						$state.go('no_appointment');*/
						
						
						$ionicLoading.hide();
						console.log(response);
						
						if(response.errors=="No Appointments or Auctions found!")
						{
						$state.go('no_appointment');
						}
						
						if(response.message=="You Do Not Have an Auction or Any Appointments")
						{
						 var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

						HomeOwners.Bid(data).then(function(response) {
							console.log("test1");
                            if (response.bids != "") {
								console.log("test2");
							$ionicHistory.clearCache();
                                storageService.save("currentBid", response);
								console.log("test3");
                             console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }
							
							 if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })

						}
						else
						{
						
						$state.go('no_appointment');
						}
						
                        
                    }
                })
		/* end client ask to move here from dasboard */
		
		
		
    }


    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }


})

/*....................  End New Auction Controller.................................... */

.controller('ResetCtrl', function($scope, $state, $http, HomeOwners) {

})

//rating controller 
.controller('RatingCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams,$ionicLoading,$ionicHistory) {

	$ionicHistory.clearCache();
    $scope.orginal = {};
    $scope.new_rating = {};
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var api_tokens = $scope.orginal.api_token;

    var approveId = $stateParams.approveId;
	
	
        var data = {
            'api_token': api_tokens
        };
	 HomeOwners.review_company_name(approveId,data).then(function(response) {
            console.log(response);
			$scope.company_name=response.company_name;
           
        });

	
    $scope.tab = 'first';
    $scope.rate = -1;
    $scope.max = 5;
    $scope.appointment_review = {};

    $scope.saveRatingToServer = function(rating) {
	
        $scope.new_rating = rating;
    };

 /*...................................... Start review functionality  .....................................*/ 
    $scope.review = function() {
	
	
	
        var new_rating = $scope.new_rating;
		
					
		if($scope.appointment_review.comments==undefined)
{
var alertPopup = $ionicPopup.alert({

                template: 'Please Enter the Review Description'
               });
             alertPopup.then(function(res) {
                 ////console.log('Thank you for not eating my delicious ice cream cone');
               });
			   $ionicLoading.hide();
}
else
{
        var data = "appointment_review[rating]=" + new_rating + "&appointment_review[text]=" + $scope.appointment_review.comments + "&api_token=" + $scope.orginal.api_token;
        //alert(data);

        HomeOwners.reviewer(data, api_tokens, approveId).then(function(response) {
            //console.log(response);
			
			/* client ask to move here from dasboard */
			var Api_token = $scope.orginal.api_token;
			HomeOwners.Homepage(Api_token).then(function(response) {
                  

                    if (typeof response.open_auction != "undefined") {


                        var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

                        HomeOwners.Bid(data).then(function(response) {

                            if (response.bids != "") {
                                storageService.save("currentBid", response);
                                ////console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }


                            if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })



                        //$state.go('my_bids');
                    } else if (typeof response.upcoming_appointments != "undefined") {
                        var upcoming_appointments = JSON.parse(response.upcoming_appointments);
                        ////console.log(upcoming_appointments);
                        storageService.save("upcoming_appointment", upcoming_appointments);
                        $ionicLoading.hide();
                        $state.go('appointment');
                 
                        
                    } else if (typeof response.completed_appointment != "undefined") {
                        $ionicLoading.hide();

                        var completed_obj = response;
                        storageService.save("completed", completed_obj);
						$state.go('lawn_cut');
                      //$state.go('appointment');
                    } else if (typeof response.dispute != "undefined") {

                        $ionicLoading.hide();
                        var disputed_obj = response;
						
                        storageService.save("disputed", disputed_obj);
						if(response.dispute_resolution_photo==null)
						{
                        $state.go('cut_in_dispute');
						}
						else
						{
						console.log(response);
							 $state.go('dispute_reply');
						}
                    } else {
                       /* $ionicLoading.hide();
						console.log(response);
						
						$state.go('no_appointment');*/
						
						
						$ionicLoading.hide();
						console.log(response);
						
						if(response.errors=="No Appointments or Auctions found!")
						{
						$state.go('no_appointment');
						}
						
						if(response.message=="You Do Not Have an Auction or Any Appointments")
						{
						 var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

						HomeOwners.Bid(data).then(function(response) {
							console.log("test1");
                            if (response.bids != "") {
								console.log("test2");
							$ionicHistory.clearCache();
                                storageService.save("currentBid", response);
								console.log("test3");
                             console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }
							
							 if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })

						}
						else
						{
						
						$state.go('no_appointment');
						}
						
                        
                    }
                })
			
			/* end client ask to move here from dasboard */
			
			
			
			
			
        });
		}

    }

/*...................................... End review functionality  .....................................*/ 

    $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }



})

/*....................  End RatingCtrl Controller.................................... */




/*.................... start DisputeReplyCtrl for  homeowner part.................................... */

.controller('DisputeReplyCtrl', function($scope, $state, HomeOwners, $ionicScrollDelegate, $ionicPopup, storageService, $stateParams,$ionicLoading) {

    $scope.orginal = {};
    $scope.tab = 'first';
    $scope.scroll_data = 0;
    $scope.rate = 3;
    $scope.max = 5;
    $scope.timestamp = [{
        'time': new Date().getTime()
    }, {
        'time': new Date().getTime() + 24 * 60 * 60 * 1000
    }];
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token,
        
    };

 
    $scope.gotourl = function(url) {
        // alert(url);
        $state.go(url);

    }
    
/*...................................... Start dispute reply functionality  .....................................*/     
    $scope.reply = function(id) {
    var api_token = storageService.get("api_token");
    $scope.orginal.api_token = JSON.parse(api_token);
    var data = {
        'api_token': $scope.orginal.api_token,
        
    };
       HomeOwners.reply_dispute(data,id).then(function(response) {
           console.log(response);
            if(response.success==true)
            {
				//$state.go("main");
				
				var Api_token = $scope.orginal.api_token;
			HomeOwners.Homepage(Api_token).then(function(response) {
                  

                    if (typeof response.open_auction != "undefined") {


                        var api_token = storageService.get("api_token");
                        $scope.orginal.api_token = JSON.parse(api_token);

                        var data = {
                            'api_token': $scope.orginal.api_token
                        };

                        HomeOwners.Bid(data).then(function(response) {

                            if (response.bids != "") {
                                storageService.save("currentBid", response);
                                ////console.log(response);
								
                                $ionicLoading.hide();
                                $state.go('my_bids');

                            } else {
                                $ionicLoading.hide();
                                $state.go('dash');
                            }


                            if (response.message == "Gary is busy getting you bids.") {
                                //alert("#")
								//$state.go('new_auction');
                                $ionicLoading.hide();
								$state.go('dash');
								
							//$state.go('approve');
								
                            }


                        })



                        //$state.go('my_bids');
                    } else if (typeof response.upcoming_appointments != "undefined") {
                        var upcoming_appointments = JSON.parse(response.upcoming_appointments);
                        ////console.log(upcoming_appointments);
                        storageService.save("upcoming_appointment", upcoming_appointments);
                        $ionicLoading.hide();
                        $state.go('appointment');
                 
                        
                    } else if (typeof response.completed_appointment != "undefined") {
                        $ionicLoading.hide();

                        var completed_obj = response;
                        storageService.save("completed", completed_obj);
						$state.go('lawn_cut');
                      //$state.go('appointment');
                    } else if (typeof response.dispute != "undefined") {

                        $ionicLoading.hide();
                        var disputed_obj = response;
						
                        storageService.save("disputed", disputed_obj);
						if(response.dispute_resolution_photo==null)
						{
                        $state.go('cut_in_dispute');
						}
						else
						{
						console.log(response);
							 $state.go('dispute_reply');
						}
                    } else {
                        $ionicLoading.hide();
						console.log(response);
						
						$state.go('no_appointment');
						
                        
                    }
                })
				
				
				
				
			}
            
        });


    }
/*...................................... End dispute reply functionality  .....................................*/     
    
    $scope.disputed = storageService.get("disputed");

    $scope.disputed_data = JSON.parse($scope.disputed);
    $scope.disputed_data_disputed_appointment = JSON.parse($scope.disputed_data.disputed_appointment);
        $scope.disputed_data_id = JSON.parse($scope.disputed_data.dispute);
    //console.log($scope.disputed_data);
    
    
    
        $scope.logout = function() {

        var api_token = storageService.get("api_token");
        var api_token = JSON.parse(api_token);
        var data = {
            'api_token': api_token
        };

        HomeOwners.logoutuser(data).then(function(response) {
            //console.log(response);

            $state.go("login");
        });

    }
    
    
})


/*.................... End  DisputeReplyCtrl for  homeowner part.................................... */







/*....................  Start Rating Directives.................................... */.directive('fundooRating', function() {
    return {
        restrict: 'A',
        template: '<ul class="rating">' + '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' + '\u2605' + '</li>' + '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            readonly: '@',
            onRatingSelected: '&'
        },
        link: function(scope, elem, attrs) {

            var updateStars = function() {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function(index) {
                if (scope.readonly && scope.readonly === 'true') {
                    return;
                }
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };

            scope.$watch('ratingValue', function(oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
        }
    }

    /*....................  End Rating Directives.................................... */
});
