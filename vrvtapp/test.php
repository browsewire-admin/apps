<?php
// Do not modify the Shipping class.
abstract class Shipping
{
    private $_itemsCount;
    private $_distance;

    public function __construct($itemsCount, $distance)
    {
        $this->_itemsCount = $itemsCount;
        $this->_distance = $distance;
    }

    abstract public function getFees();
    
    public function getDistance()
    {
        return $this->_distance;
    }
    
    public function getItemsCount()
    {
        return $this->_itemsCount;
    }
}

// You can modify code below this comment.
class InternationalShipping extends Shipping
{
    private $_internationalDistance;
    
    public function getFees(){
    return $this->_itemsCount  ;
    }
}

class LocalShipping extends Shipping
{
    
     public function getFees(){
      return $this->_itemsCount  ;
    }
} 

 "local shipping: number of items * distance * .8
international shipping: number of items * ( local distance * .8 + international distance * 1.2)";

 $items  = "{new InternationalShipping(5, 50,150), new LocalShipping(6, 35)}";

function calculateShippingFees($items) {

    // To print results to the standard output you can use print
    // Example:
    // print "Hello world!";
   //  print_r($items);
     
   foreach($items as $item){
       
       print_r($item);
   }
}

// Do NOT call the calculateShippingFees function in the code
// you write. The system will call it automatically.

?>


<div class="col col2of5">
            <section>
                <h1 class="np">PHP Frontend Developer Test v2</h1>
                <h2 class="oH2Low">Question 4 of 4</h2>
            </section>
            <article style="margin-bottom: 10px;">
                <span class="oTxtLarge">Calculate Shipping Fees</span><br>
                <span class="oNote"><p>A script is required to help calculate shipping fees for a store. The store has two types of shipping: local shipping and international shipping. They are calculated according to the following formulas:</p>
<pre><code></code></pre>
<p>A <code>LocalShipping</code> object should have <strong>number of items</strong> and <strong>local distance</strong>, respectively, as its parameters, while an <code>InternationalShipping</code> object should have <strong>number of items</strong>, <strong>local distance</strong>, and <strong>international distance</strong>, respectively.</p></span>
            </article>

            
                        <article>
                <span class="oNote"><p>Without modifying the <code>Shipping</code> class, add the necessary code so that it can be used to calculate different shipping fees. Also, create a function named <code>calculateShippingFees</code> that will be given an array of <code>Shipping</code> instances as a parameter and <strong>print</strong> the total shipping fee.</p></span>
            </article>
            
            <h3 class="oH3" style="margin-top: 20px;">Sample Input</h3>
            <article class="oWidget">
                <div class="oBd"><pre class="oTxtSmall" style="margin: 0">items: {new InternationalShipping(5, 50, 150), new LocalShipping(6, 35)}</pre></div>
            </article>
            <h3 class="oH3">Sample Output</h3>
            <article class="oWidget">
                <div class="oBd"><pre class="oTxtSmall" style="margin: 0">1268</pre></div>
            </article>

                        <h3 class="oH3">Additional Samples</h3>
            <table class="oTable">
                <thead>
                <tr>
                    <th>Input</th>
                    <th>Output</th>
                </tr>
                </thead>
                <tbody>
                                <tr>
                    <td><pre class="oTxtSmall" style="margin: 0">items: {new FastMail(6, 35)}</pre></td>
                    <td><pre class="oTxtSmall" style="margin: 0">0</pre></td>
                </tr>
                                                                </tbody>
            </table>
            
                        <article style="margin-top: 10px;">
                <h3 class="oH3">Note</h3>
                <span class="oNote"><p>*If the array has any item that is not a <code>Shipping</code> object it should set its shipping fee as <code>0</code>.</p></span>
            </article>
            
        </div>