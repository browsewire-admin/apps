<?php

 class AlertApiController extends BaseController{
   //return messages for law enforcement
   public function getMessages(){
      
	
    $ids = array('0'=>'5');

    $messages = DB::table('alerts')->whereIn('alerts_admin_role',$ids)->orderBy('created_at','DESC')->get();
	foreach($messages as $message){	 
	 $message->username = User::where('id',$message->alerts_admin_id)->select('first_name','last_name')->first();
	
	}	
	Helpers::response_data('200','Success', $messages);
   }

  //get message trail
  
  public function trailmessages(){
    $message_id  =  Input::get('messageId');
   //get message trail
   
   $messages = DB::table('alerts_messages')->where('alerts_messages_alert_id',$message_id)->get();
   
   foreach($messages as $message){
   
    $message->username = User::where('id',$message->alerts_messages_user_id)->select('first_name','last_name')->first();
	
   }
   Helpers::response_data('200','Success', $messages);
  
  }
  //reply to messages
  public function addrepliRdmessages(){
  
   $user_id     = Input::get('user_id');
   $message     = Input::get('message');
   $message_id  = Input::get('id');
   
   
   $messages['alerts_messages_alert_id'] = $message_id ;
   $messages['alerts_messages_user_id']  = $user_id;
   $messages['alerts_messages_message']  =  $message;
   
   $save =  AlertsMessages::create($messages);
   if($save){
     $messages = DB::table('alerts_messages')->where('alerts_messages_alert_id',$message_id)->get();
   
		   foreach($messages as $message){
		   
			$message->username = User::where('id',$message->alerts_messages_user_id)->select('first_name','last_name')->first();
			
		   } 
			 
     Helpers::response_data('200','Success', $messages);
     return;
   }
    else{
    Helpers::response_data('400','Some Error In saving', Null);
	}
   
  }
  //add alerts for all users
 
 public function addMessage(){
   $alert_message   = Input::get('message');
   $alerts_admin_id = Input::get('user_id');
   
   $message['alerts_admin_id'] =   $alerts_admin_id;
   $message['alerts_admin_role'] =   User::find($alerts_admin_id)->role;
   $message['alert_message'] =   $alert_message;
   
   Alerts::create($message);
   
   $this->getMessages();
   
   
  }
   
 
 }


?>