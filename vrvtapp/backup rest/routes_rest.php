<?php
/* Routes for all REST API's */

//allow cross domain requests
 App::before(function($request)
    {
        if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
            $statusCode = 204;
            $headers = [
                'Access-Control-Allow-Origin'      => '*',
                'Access-Control-Allow-Methods'     => 'GET, POST, PUT, DELETE, OPTIONS',
                'Access-Control-Allow-Headers'     => 'Content-Type'
            ];

            return Response::make(null, $statusCode, $headers);
        }
    });

    App::after(function($request, $response)
    {
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');
        return $response;
    });


 //auth related routes
 Route::group(array('prefix'=>'v1/auth'),function(){
    Route::post('firstlogin','AuthController@loginForFirstTime');
	Route::any('setpin','AuthController@SetuserPin');
    Route::post('pin','AuthController@check'); 
    Route::post('login','AuthController@login');	
 });
 
 //vehicle search related routes
 Route::group(array('prefix'=>'v1'),function(){
    Route::post('search/vehicle','VehicleSearchApiController@advance');
	Route::post('vehicle/detail','VehicleSearchApiController@Detail'); 
	Route::post('vehicle/country','VehicleSearchApiController@country');
    Route::post('messages','AlertApiController@getMessages');	
	Route::post('messages/trail','AlertApiController@trailmessages');
	Route::post('messages/trail/reply','AlertApiController@addrepliRdmessages'
	);
	Route::post('messages/main','AlertApiController@addMessage');
 });
 
 //adding authorized driver api's
 Route::group(array('prefix'=>'v1/driver'),function(){
   Route::post('country','DriverApiController@country');
   Route::post('status','DriverApiController@status');
   Route::post('register','DriverApiController@register');
 });
 
 //routes for fine control
 
 Route::group(array('prefix'=>'v1/fines'),function(){
   Route::post('gettype','FineApiController@getfinetype');
   Route::post('getlist','FineApiController@getfinelist');
   Route::post('issuefine','FineApiController@issuefine');
   Route::post('search','FineApiController@searchfine');
   
 });
 
 //Route::resource('auth','AuthController');


?>