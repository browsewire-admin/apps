<?php
class VehicleSearchApiController extends BaseController{
 
  public function advance(){
     //ADVANCE VEHICLE SEARCH 	
     $case = Input::get('case');	
	 switch($case){
	  case 1 :
	  //case for registration number
		   $registration_number = trim(Input::get('reg_number'));
			
			$vehicle_id =    Registration::where('registrations_no',$registration_number)->lists('registrations_vehicle_id');
			if(empty($vehicle_id)){
			Helpers::response_data('404','No record found', NULL);
			return;
			}
			$vehicle_data = Vehicle::whereIn('id',$vehicle_id)->first();
			
			//send data to helpers for sending response 
			Helpers::search_data($vehicle_data);
			
			
        break;		
	  case 2 :	   
		   //case for licence number
		    $lic_number = trim(Input::get('lic_number'));
			
			$vehicle_id =    Registration::where('registrations_licence_plate_no',$lic_number)->lists('registrations_vehicle_id');
			if(empty($vehicle_id)){
			Helpers::response_data('404','No record found', NULL);
			return;
			}
			$vehicle_data = Vehicle::whereIn('id',$vehicle_id)->first();
			
			//send data to helpers for sending response 
			Helpers::search_data($vehicle_data);		
        break;	
      
      case 3 :
	  
		//case for licence number
		    $sticker_number = trim(Input::get('sticker_number'));
			
			$vehicle_id =    Registration::where('registrations_sticker_serial_no',$sticker_number)->lists('registrations_vehicle_id');
			if(empty($vehicle_id)){
			Helpers::response_data('404','No record found', NULL);
			return;
			}
			$vehicle_data = Vehicle::whereIn('id',$vehicle_id)->first();
			
			//send data to helpers for sending response 
			Helpers::search_data($vehicle_data);
        break;	
      case 4 :
	  
	    $licence_number = trim(Input::get('driver_lic_number')); 
        $driver = Driver::where('drivers_license_no',$licence_number)->first();
        if(!$driver){
         Helpers::response_data('404','Driver Not Found.', NULL);
         return;
		 }
		 
		 Helpers::DriverResponse($driver);
      		
		
        break;
      default:		
	    Helpers::response_data('404','Something went Wrong.', NULL);
		break;
	 
	 }
	
  }
  
  //API function to return vehicle detail
  public function Detail(){
         $id = Input::get('id');   
		
        $vehicle = Vehicle::withTrashed()->find($id);
		$registration = $vehicle->registration;
		
		$vehicle['vehicles_class_name'] = Vehicle::vehicleClassName($id);
		$vehicle['vehicles_color_name'] = Color::find($vehicle->vehicles_color)->colors_name;
		$vehicle['vehicles_status_name'] = VehicleStatus::find($vehicle->vehicles_status)->vehicle_status_name;
		$vehicle['vehicles_usetype_name'] = VehicleUseType::find($vehicle->vehicles_use_type)->vehicle_use_types_name;
		//vehicle owner
		$owners = $vehicle->owners;
		$driverowner = array();
		foreach($owners as $owner){
		 $owner->status = DrivingStatus::find($owner->drivers_driving_status)->driving_status_name ;
		 array_push($driverowner, array('id'=>$owner->id,'name'=>$owner->drivers_fname ." ". $owner->drivers_lname));
		 //get driver fines
		 $fine_driver_count =  ViolateDriver::where('driver_id',$owner->id)->count();
		 if(($fine_driver_count)){
		 $fine_driver  = ViolateDriver::where('driver_id',$owner->id)->select(array('driver_id','created_at','driver_fine_id'))->get();
		  foreach($fine_driver as $fines){
		   $fines->code= DriverFine::find($fines->driver_fine_id)->driver_violation_code;
		  
		 }
		 
		 }else
		  { 
		    $fine_driver = NULL;
		 
		   }
		
		 $owner->fines = $fine_driver;
	
		
		}
		
		//authorised driver
		$authorizedDr = $vehicle->authorizedDrivers;
		$authoriseddrivers =array(); 
		foreach($authorizedDr as $owner){
		 $owner->status = DrivingStatus::find($owner->drivers_driving_status)->driving_status_name ;
		 array_push($authoriseddrivers, array('id'=>$owner->id,'name'=>$owner->drivers_fname ." ". $owner->drivers_lname));
		 //get driver fines
		 $fine_driver_count =  ViolateDriver::where('driver_id',$owner->id)->count();
		 if(($fine_driver_count)){
		 $fine_driver  = ViolateDriver::where('driver_id',$owner->id)->select(array('driver_id','created_at','driver_fine_id'))->orderby('created_at','desc')->take(2)->get();
		  foreach($fine_driver as $fines){
		   $fines->code= DriverFine::find($fines->driver_fine_id)->driver_violation_code;
		 }
		 
		 }else
		  { 
		    $fine_driver = NULL;
		 
		   }
		
		 $owner->fines = $fine_driver;
	
		
		}
		//get vehicle fines
		$fines_vehicle =  ViolateVehicle::where('vehicle_id',$id)->take(2)->get();
		   if(empty($fines_vehicle)){
		    $fines_vehicle= NULL;
		   }else
		    {
			  foreach($fines_vehicle as $fines){
			  $fines->code = VehicleFine::find($fines->vehicle_fine_id)->vehicle_violation_code;			  
			  }			 
			}
			
		$vehicle->fines = $fines_vehicle;	
			
		
		//$fines = ViolateVehicle::where('vehicle_id',$id)->get();
		//print_r($fines);
		
		$authorizedDr = $vehicle->authorizedDrivers;
		
		$all_drivers =  array_merge($driverowner,$authoriseddrivers);
		$result_all =  array(
							'vehicle'=>$vehicle,
                            'drivers'=>$all_drivers,							
							 );
		
		Helpers::response_data('200','Success', $result_all);
		
  
  }


}


?>