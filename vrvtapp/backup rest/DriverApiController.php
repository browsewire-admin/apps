<?php 

 class DriverApiController extends BaseController{
  
  
  //get country data  
  public function country(){
	  $country_name = DB::table('countries')->lists('country_name'); 
	  //print_r($country_name);
	  Helpers::response_data('200','Success', $country_name);
  }
 
  //get driver status data  
  public function status(){
	  $driver_status = DB::table('driving_status')->lists('driving_status_name'); 
	  
	  Helpers::response_data('200','Success', $driver_status);
  }
  
  //get driver status data  and register them
  public function register(){
	  $input = Input::json();	 
	  //check if user with this license number exist
	  $authorized_license_no = $input->get('dlic');
	  $authorized_status = Driver::where('drivers_license_no', $authorized_license_no)->count();
   
				//IF DRIVER DOESN'T EXISTS, CREATE DRIVER
				if($authorized_status == NULL){
                    $country_id = DB::table('countries')->where('country_name',$input->get('country'))->first()->id;
				      
				    $status_id = DB::table('driving_status')->where('driving_status_name',$input->get('status'))->first()->id;
					$authorized_info['drivers_fname']          = $input->get('fname');
					$authorized_info['drivers_mname']          = $input->get('mname');
					$authorized_info['drivers_lname']          = $input->get('lname');
					$authorized_info['drivers_nin']            = $input->get('nin');
					$authorized_info['drivers_license_no']     = $input->get('dlic');
					$authorized_info['drivers_street']         = $input->get('street');
					$authorized_info['drivers_city']           = $input->get('city');
					$authorized_info['drivers_po_box']         = $input->get('po');
					$authorized_info['drivers_country']        = $country_id;
					$authorized_info['drivers_cell1']          = $input->get('cell1');
					$authorized_info['drivers_cell2']          = $input->get('cell2');
					$authorized_info['drivers_email']          = $input->get('email');
					$authorized_info['drivers_driving_status'] = $status_id;

					$insert_authorized_into_driver_table = Driver::create($authorized_info);
					$authorized_ids = $insert_authorized_into_driver_table->id;
					
				}
				// IF DRIVER EXISTS, GET HIS ID
				else {
					$authorized_ids = Driver::where('drivers_license_no', $authorized_license_no)->first()->id;
					//check if driver exist and already added to vehicle. 
				   $checkIfdriver=VehicleDriverAuthorized::where('vehicle_driver_authorized_driver_id',$authorized_ids)->where('vehicle_driver_authorized_vehicle_id',$input->get('vehicle_id'))->count();
				   if($checkIfdriver){
				    Helpers::response_data('200','Already Added to this vehicle',"saved" );
					return;
				   }
				} 
				
				$authorized_ids_fet = (int) $authorized_ids;
				
				Vehicle::find($input->get('vehicle_id'))->authorizedDrivers()->attach($authorized_ids_fet);
				
      		    Helpers::response_data('200','Success',"saved" );
 	         
     

  }
 }

?>