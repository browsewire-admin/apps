<?php

class AuthController extends \BaseController {
  
   
   public function index(){
     
     return "index";
   }
   
   
   public function show($id){
    
	 echo $id;
	 
   }
   
   //login user for first time   
   public function loginForFirstTime(){  
	 $email    = Input::get('email');
	 $password = Input::get('password');
	 
	 //check if user email exist and send response	 
	 $user_check_count = User::where('email',$email)->count();
	 
	 if($user_check_count){
	  //if user matches check password
	    $user_data = User::where('email',$email)->first();
	    if (Hash::check($password , $user_data->password)){
	     //check user role it should be law enforcement only
		   if($user_data->role != '1'){
		       Helpers::response_data('403','Only law Enforcemnt user can access this, Contact Superadmin for more info..', NULL);
		   }else
		      {
		         Helpers::response_data('200','Success', $user_data);
		      }
		} else
               {			   
			     Helpers::response_data('403','Password mismatched for this email!!', NULL);			   
			   }
	 }
	 else{
	 
	   //send reponse if user pass and email does't match
	   Helpers::response_data('404','User email doesn\'t exist', NULL);
	 }
   }
   
   //set user pin   
   public function SetuserPin(){
    //get pin and email
	$email = Input::get('email');
	$pin =  Input::get('pin');
	
	$user_data =  User::where('email',$email)->first();
	
	$user = User::find($user_data->id);
	$user->mobile_pin = $pin;	
	if($user->save()){
	   Helpers::response_data('200','success', NULL);
	
	}else
	    {
	       Helpers::response_data('403','Some error occurred ,try again', NULL);
     	}
   }
   
   //checking mobile pin and return user name
   public function check(){
    //get mobile pin
     $mobile_pin = Input::get('pin');	
	//if not empty
    if(!empty($mobile_pin)){
	//check pin exist or not
	 $check_pin_exist = User::where('mobile_pin',$mobile_pin)->count();
	     if($check_pin_exist){
	       $user_data_pin_based = User::where('mobile_pin',$mobile_pin)->select('first_name','middle_name','last_name')->get();  
	       Helpers::response_data('200','success',$user_data_pin_based);     
	     }else{
              Helpers::response_data('404','User not found', NULL);
         }	  
	
	}else{
	    Helpers::response_data('403','user not allowed', NULL);
	
	}
	
   }
   //login user by checking his badge id and pin
   public function login(){     
	 $user_badge_id  = Input::get('badgeid');
	 $user_pin  = Input::get('pin');
	 //check if badge id and pin are of same user
	 $user = User::where('badge_id_number',$user_badge_id)->where('mobile_pin',$user_pin)->count();
	 
     if(!empty($user)){
	 
	  $user_data = User::where('badge_id_number',$user_badge_id)->where('mobile_pin',$user_pin)->first();
	   
	  Helpers::response_data('200','success', $user_data);
	 }
	 else{
       Helpers::response_data('404','user not found',NULL);
     }
	 //echo json_encode($user);
    
   
   }
   
   

}


?>