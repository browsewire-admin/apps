<?php
 class FineApiController extends BaseController{
    
	//get all fines category as selected by user
    public function getfinetype(){
	 
	  $type = Input::get('data');	  
	  switch($type){
	   case 'vehicle':
	    $fines = DB::table('vehicle_violation_types')->lists('vehicle_violation_type');
		Helpers::response_data('200','Success', $fines);
		break;
		
	   case 'driver':
	    $fines = DB::table('driver_violation_types')->lists('driver_violation_type');
		Helpers::response_data('200','Success', $fines);
        break;		
	  
	  }
	}
	
	//get selected fine list	
	public  function getfinelist(){
	 
	  $type = Input::get('type');
	  $name = Input::get('name');
	  
	  switch($type){
	  case 'vehicle':	  
	  $fine_id = DB::table('vehicle_violation_types')->where('vehicle_violation_type',$name)->first()->id;
	  $fine_options =  VehicleFine::where('vehicle_violation_type_id',$fine_id)->lists('vehicle_violation');
	  Helpers::response_data('200','Success', $fine_options);
	  break;
	  
	  case 'driver':
	  $fine_id = DB::table('driver_violation_types')->where('driver_violation_type',$name)->first()->id;
	  $fine_options =  DriverFine::where('driver_violation_type_id',$fine_id)->lists('driver_violation');
	  Helpers::response_data('200','Success', $fine_options);
	  break;
	  
	  }  
	
	}
	
	//get fines data and save accordingly

   public function issuefine(){
     //get object response
	 
	$input =  Input::json();
    
	//latitude and longitude details
	$latitude =  $input->get('latitude');
	$longitude=  $input->get('longitude');
	//fined by user 
	$officer_data = $input->get('finedby');
	
	//vehicle id , driver id and fines 
	
	$finedata   = $input->get('fine');
	$driver_id  = $finedata['driverId'];
    $vehicle_id = $finedata['vehicleId'];
	$fines_name = $finedata['fines'];
	//GROUP ALL FINES NAME ACCORDING TO USER TYPE
	foreach($fines_name as $names){	
	 $data[$names['type']][]= $names['name'];	
	}
    $today = date('mdY');
	
	if(is_numeric($driver_id)){
	
	 $driverfines = $data['driver'];
     //print_r($driverfines);	 
	 $fine_id = DriverFine::whereIn('driver_violation',$driverfines)->lists('id');
	
	 $fine = DriverFine::find($fine_id[0]);
	 $sequence = ViolateDriver::orderBy('id','DESC')->first();
	 $sequence_id = $sequence->id + 1001;
    //citation number	
	 $citaion_number = $fine->driver_violation_code.'-'.$today.'-'.$officer_data['id'].'-'.$sequence_id;
	 
	 //SAVE FINES FOR DRIVERS
	 foreach($fine_id as $id){	 
	 $fineinfo['driver_id']       =    $driver_id;
	 $fineinfo['vehicle_id']      =    $vehicle_id;
	 $fineinfo['driver_fine_id']  =    $id;
	 $fineinfo['driver_fine_unique_id'] = $citaion_number;
	 $fineinfo['officer_id']      =   $officer_data['id'];
	 $fineinfo['longitude']       =   $longitude;
     $fineinfo['latitude']        =   $latitude;
     $fineinfo['status']          =	  '2';	 
	 
	 ViolateDriver::create($fineinfo);
	 
	 }
	 
	//SEND EMAIL HERE
	$title = "New traffic citation";
	
	$registration_number = DB::table('registrations')->where('registrations_vehicle_id', $vehicle_id)->first()->registrations_no;
	$driverfines = $data['driver'];	
    $amount =  array_sum(DriverFine::whereIn('id',$fine_id)->lists('driver_fine_ammount'));
	$driver = Driver::find($driver_id);
	
	$owner_ids =  DB::table('vehicle_driver_owners')->where('vehicle_driver_owners_vehicle_id',$vehicle_id)->lists('vehicle_driver_owners_driver_id');
	
	$owners =  Driver::whereIn('id',$owner_ids)->get()->toArray();
	$owners_email = Driver::whereIn('id',$owner_ids)->lists('drivers_email');
	$driver_email = Driver::where('id',$driver_id)->lists('drivers_email');
    
	$sender = array_merge($owners_email,$driver_email);	
	$sender = array_unique($sender);
	$sent = Mail::send('emails.finecitation', array('registration_number'=>$registration_number,'fine_descp'=>$driverfines,'citation_num'=>$citaion_number,'amount'=>$amount,'driver'=>$driver,'owners'=>$owners,'type'=>'driver'), function($message) use ($title,$sender)
			{  
				$message->to($sender)->subject($title);
			});
	  
	
	//CHECK FOR HIS YEAR FINE POINT AND SEND EMAIL IF NECESSARY	 
	 $driver_fine_inyear = ViolateDriver::where('driver_id',$driver_id)->lists('driver_fine_id','created_at');
	 //print_r($driver_fine_inyear);
	 
	}
	 if(array_key_exists('vehicle', $data)){
	     $vehiclefine = $data['vehicle'];
	   //check if we have vehicle violations names	 
	    $fine_id = VehicleFine::whereIn('vehicle_violation',$vehiclefine)->lists('id');
		$fine = VehicleFine::find($fine_id[0]);
		 $sequence = ViolateVehicle::orderBy('id','DESC')->first();
		 $sequence_id = $sequence->id + 1001;
		//citation number	
		 $citaion_number = $fine->vehicle_violation_code.'-'.$today.'-'.$officer_data['id'].'-'.$sequence_id;
		 //SAVE FINES FOR DRIVERS
		 foreach($fine_id as $id){
		 //echo $id;
		 $fineinfo['driver_id']        =    $driver_id;
		 $fineinfo['vehicle_id']       =    $vehicle_id;
		 $fineinfo['vehicle_fine_id']  =    $id;
		 $fineinfo['vehicle_fine_unique_id'] = $citaion_number;
		 $fineinfo['officer_id']      =  $officer_data['id'];
		 $fineinfo['longitude']       =   $longitude;
		 $fineinfo['latitude']        =  $latitude;
		 $fineinfo['status']          =	  '2';
		 
		 ViolateVehicle::create($fineinfo);
		 }
	 
	  //SEND EMAIL HERE
		$title = "New traffic citation";
		
		$registration_number = DB::table('registrations')->where('registrations_vehicle_id', $vehicle_id)->first()->registrations_no;
		
		$amount =  array_sum(VehicleFine::whereIn('id',$fine_id)->lists('vehicle_fine_ammount'));
		
		$owner_ids =  DB::table('vehicle_driver_owners')->where('vehicle_driver_owners_vehicle_id',$vehicle_id)->lists('vehicle_driver_owners_driver_id');
		
		$owners = Driver::whereIn('id',$owner_ids)->get()->toArray();
		$owners_email = Driver::whereIn('id',$owner_ids)->lists('drivers_email');
		
		$sender = array_unique($owners_email);
		$sent = Mail::send('emails.finecitation', array('registration_number'=>$registration_number,'fine_descp'=>$vehiclefine,'citation_num'=>$citaion_number,'amount'=>$amount,'owners'=>$owners,'type'=>'vehicle'), function($message) use ($title,$sender)
				{  
					$message->to($sender)->subject($title);
				});
	  
	 
	   }
	  
	     Helpers::response_data('200','Success',"saved");
   }  

  //search fines

  public function searchfine(){
 
  $type = Input::get('type');
  switch($type){

	 case 'vehicle':
		  $citation_name = Input::get('fineid'); 
		  $finedetail = ViolateVehicle::where('vehicle_fine_unique_id',$citation_name)->count();
		  if(!($finedetail)){
			  Helpers::response_data('404','No result found', Null);
			  return;
		  }
		  $finedetail = ViolateVehicle::where('vehicle_fine_unique_id',$citation_name)->get();
		  $this->sendsearch($finedetail,'vehicle');
		  break;
     case 'driver':
		  $citation_name = Input::get('fineid'); 
		  $finedetail = ViolateDriver::where('driver_fine_unique_id',$citation_name)->count();
		  if(!($finedetail)){
			  Helpers::response_data('404','No result found', Null);
			  return;
		  }
		  $finedetail = ViolateDriver::where('driver_fine_unique_id',$citation_name)->get();
		  $this->sendsearch($finedetail,'driver');
	 	  break;
    }    
  
  }   
 
 //common function for both driver and vehicle search result
	public function sendsearch($finedetail,$type){
	
	foreach($finedetail as $fines){
	 $registration_number = DB::table('registrations')->where('registrations_vehicle_id', $fines->vehicle_id)->first()->registrations_no;
	 
	 $created_at = $fines->created_at;
	 
	 $driver = Driver::where('id',$fines->driver_id)->select('drivers_fname','drivers_lname')->first();
	 $status_name = DB::table('fine_status')->where('id',$fines->status)->first()->fine_status;
	 if($type === 'vehicle'){
	  $fine['fine_name'][] = VehicleFine::where('id',$fines->vehicle_fine_id)->first()->vehicle_violation;
	  
	  
	 }
	 else if($type === 'driver'){
	
	 $fine['fine_name'][] =  DriverFine::where('id',$fines->driver_fine_id)->first()->driver_violation;
	 }
	 $officer = User::where('id',$fines->officer_id)->select('first_name','last_name')->first();
	}
	
	$results =  array('registration'=>$registration_number,'driver'=>$driver,'fines'=>$fine,'issued_at'=>$created_at,'status'=>$status_name,'officer'=>$officer);
  
   	Helpers::response_data('200','Success', $results);
  }
   
 
 }



?> 